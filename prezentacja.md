---
author: Michał Dytkowski
title: Badminton
subtitle: Beamer
date: 07.11.2021
theme: Warsaw
output: beamer_presentation
header-includes:
    \usepackage{xcolor}
    \usepackage{listings}
---



## Slajd 1 -SPIS TREŚCI

1. Na czym polega gra?
2. Historia badmintona
3. Badminton na olimpiadzie
4. Polscy zawodnicy

## Slajd 2 - Na czym polega gra?

Zasady:
1. Gra polega na przebijaniu nad siatką lotki za pomocą
rakietek.
2. Gra rozgrywana jest na punkty na boisku o długości
13,40 m i szerokości 6,10 m w deblu lub 5,18 w singlu.
3. Linie wyznaczają pola serwisowe i odbioru (pole odbioru
w grze podwójnej jest o 0,76 m krótsze)
4. Siatka o szerokości 76 cm zawieszona jest na wysokości
1,55 m przy słupkach i 1,524 m na środku



## Slajd 3 - Na czym polega gra?

5. Lotka o masie od 4,74 g do 5,50 g wykonana jest z
korka pokrytego warstwą skóry i 16 piórek naturalnych
lub z materiałów syntetycznych
6. Klasyczny mecz składa się z setów rozgrywanych do 21
punktów (wymagana jest przewaga dwóch punktów,
jeżeli strony osiągną wynik 29:29 to seta wygrywa strona
zdobywająca 30. punkt).
7. Gra kończy się, gdy jeden z zawodników lub jedna z
drużyn wygrywa dwa sety.



## Slajd 4 - Historia badmintona
Nazwa gry pochodzi od nazwy angielskiej posiadłości
(Badminton House) VIII księcia Beaufort, leżącej w
hrabstwie Gloucestershire niedaleko Bristolu, która
nazywała się właśnie Badminton. To tam około roku
1870 odbył się pierwszy pokaz gry opartej na podbijaniu
rakietką lotki wykonanej z korka i piór z zastosowaniem
reguł zbliżonych do współczesnych. Po kilku latach
przyjęto jednolity zbiór przepisów gry, w których do
dzisiaj niezmienne pozostały długość i szerokość boiska
(w rzeczywistości były to wymiary salonu, w którym
odbył się pierwszy pokaz) oraz wysokość siatki. Z
biegiem czasu gra stawała się coraz bardziej popularna.
Zyskała sympatię nie tylko w gronie amatorów, lecz
także i profesjonalistów. Z tego to właśnie powodu
powołano Międzynarodową Federację Badmintona
(MFB).


## Slajd 5 - Historia badmintona
Na świecie badminton najbardziej popularny jest w
krajach wschodniej Azji (Chiny, Japonia, Korea itd.)
oraz w Danii. W Europie, oprócz Danii, jego główne
ośrodki znajdują się w Wielkiej Brytanii, Niemczech i
Szwecji.
Odbywające się co roku Mistrzostwa świata w
badmintonie po raz pierwszy zostały rozegrane w
szwedzkim Malm¨o w 1977 roku. Oprócz indywidualnych
mistrzostw świata rozgrywane są również drużynowe
mistrzostwa mężczyzn (Thomas Cup) oraz kobiet (Uber
Cup).


## Slajd 6 - Badminton na olimpiadzie
Olimpijska kariera badmintona rozpoczęła się w roku
1988 na Letnich Igrzyskach Olimpijskich w Seulu, gdzie
grę potraktowano pokazowo. [por. Badminton na letnich
igrzyskach olimpijskich] Badminton bardzo szybko stał
się modny i już na Igrzyskach w Barcelonie w 1992 roku
włączono go do programu jako dyscyplinę medalową. Do
zdobycia były wtedy tylko dwa medale – złoty i srebrny.
Możliwość zdobycia brązu pojawiła się dopiero w
Atlancie w 1996 roku. W Atenach badminton
rozgrywany był w pięciu konkurencjach:
1. gra pojedyncza kobiet
2. gra pojedyncza mężczyzn
3. gra podwójna kobiet
4. gra podwójna mężczyzn
5. gra mieszana

## Slajd 7 - Polscy zawodnicy
Najlepsi polscy zawodnicy to:
1. Robert Mateusiak
2. Michał Łogosz
3. Przemysław Wacha
4. Kamila Augustyn
5. Nadieżdża Zięba


## Slajd 8 - BIBLIOGRAFIA
* Wikipedia (https://pl.wikipedia.org/wiki/Badminton)


## Slajd 9 - Koniec
# **DZIEKUJE ZA UWAGĘ**
